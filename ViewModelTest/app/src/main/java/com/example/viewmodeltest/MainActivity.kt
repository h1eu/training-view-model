package com.example.viewmodeltest



import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.viewmodeltest.databinding.ActivityMainBinding
import com.example.viewmodeltest.viewmodel.CounterViewModel
import com.example.viewmodeltest.viewmodel.CounterViewModelFactory


class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var mCounterViewModel : CounterViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        createViewModel()
        displayCount()
        handle()
    }



    private fun createViewModel() {
        var factory = CounterViewModelFactory(10)
        mCounterViewModel = ViewModelProvider(this,factory).get(CounterViewModel::class.java)
    }

    private fun handle() {
        activityMainBinding.imgUp.setOnClickListener(View.OnClickListener {
            mCounterViewModel.count++
            displayCount()
        })
        activityMainBinding.imgDown.setOnClickListener(View.OnClickListener {
            mCounterViewModel.count--
            displayCount()
        })
    }


    private fun displayCount() {
        activityMainBinding.tvNumber.text = mCounterViewModel.count.toString()
    }
}